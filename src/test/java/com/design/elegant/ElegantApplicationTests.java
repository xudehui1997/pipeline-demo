package com.design.elegant;

import com.design.elegant.charge.ChargeRequest;
import com.design.elegant.service.IChargeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ElegantApplicationTests {

  @Autowired
  private IChargeService chargeService;

  @Test
  void contextLoads() {
  }


  @Test
  public void testCharge(){
    ChargeRequest request = new ChargeRequest();
    request.setBizCode("YW1");
    request.setCarNo("1234");
    chargeService.handle(request);

//    request.setBizCode("YW1");
//    request.setCarNo("2345");
//    request.setUserId(1L);
//    chargeService.handle(request);
  }

  @Test
  public void testWithAnno(){
    ChargeRequest request = new ChargeRequest();
    request.setBizCode("YW3");
    request.setCarNo("2343");
    request.setAddressId(124L);
    chargeService.handleWithAnno(request);
  }

}
