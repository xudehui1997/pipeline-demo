package com.design.elegant.charge;

import lombok.Data;

@Data
public class User {

  private Long id;
  private String username;

  private String email;
}
