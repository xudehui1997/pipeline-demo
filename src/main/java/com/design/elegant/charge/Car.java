package com.design.elegant.charge;

import lombok.Data;

@Data
public class Car {

  private String carNo;

  private String vinNO;

}
