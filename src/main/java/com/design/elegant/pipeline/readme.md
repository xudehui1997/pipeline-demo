责任链模式
- 通过配置处理器的名字来实现动态的逻辑增减

- priority默认为0，过滤器指定priority后，会按照从大到小的顺序执行，没有指定priority的过滤器，则按照添加顺序执行

- 问题
  - 如果ChargeServiceImpl被标记为@Service注解，表示它是一个由Spring管理的组件，那么您可以继续使用依赖注入方式将FilterFactory注入到ChargeServiceImpl中，而无需手动创建FilterFactory实例。 
  - 在这种情况下，您需要确保FilterFactory类上仍然保留@Component注解，以便让Spring能够自动扫描并创建FilterFactory的实例。

- 问题：如果就是不想在 FilterFactory 上有 @Component 注解，但仍然希望在`ChargeServiceImpl`中使用`FilterFactory`，您可以通过其他方式将其注入到`ChargeServiceImpl`中。

  - 一种方法是使用`@Configuration`注解创建一个配置类，在配置类中手动创建`FilterFactory`的实例，并将其声明为一个`@Bean`：

```java
@Configuration
public class AppConfig {

  @Bean
  public FilterFactory filterFactory() {
    return new FilterFactory();
  }

  // ... 其他配置 ...
}
```

- 然后，在`ChargeServiceImpl`中使用`@Autowired`注解将`FilterFactory`注入：

```java
@Service
public class ChargeServiceImpl implements IChargeService {

  private final FilterFactory filterFactory;

  @Autowired
  public ChargeServiceImpl(FilterFactory filterFactory) {
    this.filterFactory = filterFactory;
  }

  // ... 省略其他代码 ...
}
```

- 通过这种方式，您可以在`ChargeServiceImpl`中使用不带`@Component`注解的`FilterFactory`实例，并进行依赖注入。`FilterFactory`实例将由Spring根据配置类中的`@Bean`方法创建并注入到`ChargeServiceImpl`中。
请确保在创建 `ChargeServiceImpl` 实例时，配置类已经被加载，并且`FilterFactory`实例可以被正确创建和注入。
  - 