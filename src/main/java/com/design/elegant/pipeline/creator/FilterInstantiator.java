package com.design.elegant.pipeline.creator;

import com.design.elegant.pipeline.EventFilter;
import com.design.elegant.pipeline.annotation.EventFilterAnnotation;
import com.design.elegant.pipeline.config.FilterConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author coderxdh
 * @date 2023/12/10 22:13
 */
@Component
public class FilterInstantiator {

    private final ApplicationContext applicationContext;

    @Autowired
    public FilterInstantiator(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public EventFilter createFilter(FilterConfiguration.FilterDefinition definition) {
        try {
            EventFilter filter = (EventFilter) applicationContext.getBean(Class.forName(definition.getName()));
            filter.setPriority(definition.getPriority());
            return filter;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Failed to create filter instance for: " + definition.getName(), e);
        }
    }

    public EventFilter createFilter(Class<?> filterClass) {
        EventFilterAnnotation annotation = filterClass.getAnnotation(EventFilterAnnotation.class);
        int priority = annotation.priority();
        EventFilter filter = (EventFilter) applicationContext.getBean(filterClass);
        filter.setPriority(priority);
        return filter;
    }
}
