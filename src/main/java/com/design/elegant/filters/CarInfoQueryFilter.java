package com.design.elegant.filters;

import com.design.elegant.charge.Car;
import com.design.elegant.charge.ChargeContext;
import com.design.elegant.charge.ChargeModel;
import com.design.elegant.charge.ChargeRequest;
import com.design.elegant.pipeline.AbstractEventFilter;
import com.design.elegant.service.IFacadeService;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
public class CarInfoQueryFilter extends AbstractEventFilter<ChargeContext> {

  @Resource
  private IFacadeService facadeService;

  @Override
  protected void handle(ChargeContext context) {
    log.info("{}-current-thread-name: {}", this.getClass().getSimpleName(), Thread.currentThread().getName());
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    if(Objects.isNull(context.getCar())){
      ChargeRequest chargeRequest = context.getChargeRequest();
      String carNo = chargeRequest.getCarNo();
      Car car = facadeService.getCarInfoByCarNO(carNo);
      context.setCar(car);
      ChargeModel chargeModel = new ChargeModel();
      context.setChargeModel(chargeModel);
    }
    log.info("查询车辆信息并且放入上下文中");
  }
}
