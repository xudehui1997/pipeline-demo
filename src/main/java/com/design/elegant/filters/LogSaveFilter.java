package com.design.elegant.filters;

import com.design.elegant.charge.ChargeContext;
import com.design.elegant.pipeline.AbstractEventFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class LogSaveFilter extends AbstractEventFilter<ChargeContext> {

  @Override
  protected void handle(ChargeContext context) {
    log.info("{}-current-thread-name: {}", this.getClass().getSimpleName(), Thread.currentThread().getName());
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    log.info("请求存储，发送到mq");
  }
}
