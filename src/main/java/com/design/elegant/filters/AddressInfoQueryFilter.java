package com.design.elegant.filters;

import com.design.elegant.charge.*;
import com.design.elegant.pipeline.AbstractEventFilter;
import com.design.elegant.pipeline.annotation.EventFilterAnnotation;
import com.design.elegant.service.IFacadeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

@Slf4j
@Component
@EventFilterAnnotation(bizCode = "YW3", priority = 2)
public class AddressInfoQueryFilter extends AbstractEventFilter<ChargeContext> {

  @Resource
  private IFacadeService facadeService;

  @Override
  protected void handle(ChargeContext context) {
    log.info("{}-current-thread-name: {}", this.getClass().getSimpleName(), Thread.currentThread().getName());
    if(Objects.isNull(context.getAddress())){
      ChargeRequest chargeRequest = context.getChargeRequest();
      Long addressId = chargeRequest.getAddressId();
      Address address = facadeService.getAddressByAddressId(addressId);
      context.setAddress(address);
      ChargeModel chargeModel = new ChargeModel();
      context.setChargeModel(chargeModel);
    }
    log.info("查询地址信息并且放入上下文中");
  }
}
