package com.design.elegant.service;

import com.design.elegant.charge.ChargeRequest;

public interface IChargeService {

  void handle(ChargeRequest chargeRequest);

  void handleWithAnno(ChargeRequest chargeRequest);

}
