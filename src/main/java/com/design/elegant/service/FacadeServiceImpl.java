package com.design.elegant.service;

import com.design.elegant.charge.Address;
import com.design.elegant.charge.Car;
import org.springframework.stereotype.Service;

@Service
public class FacadeServiceImpl implements IFacadeService{

  @Override
  public Car getCarInfoByCarNO(String carNo) {
    Car car = new Car();
    car.setCarNo(carNo);
    return car;
  }

  @Override
  public Address getAddressByAddressId(Long addressId) {
    Address address = new Address();
    address.setId(addressId);
    return address;
  }
}
